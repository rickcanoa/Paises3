﻿using Paises.Models;
using System.Collections.ObjectModel;
using System.Linq;
using Xamarin.Forms;

namespace Paises.ViewModel
{
    public class PaisViewModel : BaseViewModel
    {
        #region Atributos
        private ObservableCollection<Border> borders;
        private ObservableCollection<Currency> currencies;
        private ObservableCollection<Language> languages;
        private Pais _pais;
        private string _res;
        #endregion

        #region Propiedades
        public string Res
        {
            get { return _res; }
            set
            {
                if (_res != value)
                {
                    _res = value;
                    OnPropertyChanged(nameof(Res));
                }
            }
        }
        public Pais ObjPais
        {
            get { return _pais; }
            set
            {
                if (_pais != value)
                {
                    _pais = value;
                    OnPropertyChanged(nameof(ObjPais));
                }
            }
        }
        public ObservableCollection<Border> Borders
        {
            get { return this.borders; }
            set
            {
                if (borders != value)
                {
                    borders = value;
                    OnPropertyChanged(nameof(Borders));
                }
            }
        }

        public ObservableCollection<Currency> Currencies
        {
            get { return this.currencies; }
            set
            {
                if (currencies != value)
                {
                    currencies = value;
                    OnPropertyChanged(nameof(Currencies));
                }
            }
        }

        public ObservableCollection<Language> Languages
        {
            get { return this.languages; }
            set
            {
                if (languages != value)
                {
                    languages = value;
                    OnPropertyChanged(nameof(Languages));
                }
            }
        }
        #endregion

        #region Constructor
        public PaisViewModel(Pais pais)
        {
            ObjPais = pais;
            Res = "HOLA MUNDO CODE";
            
            loadPias();
            this.LoadBorders();
        }

        private async void loadPias()
        {
            //await Application.Current.MainPage.DisplayAlert("Mensaje", ObjPais.Name, "Aceptar");

        }
        #endregion

        #region Metodos
        private void LoadBorders()
        {
            this.Borders = new ObservableCollection<Border>();
            foreach (var border in this.ObjPais.Borders)
            {
                //var land = ObjPais.Borders.
                //                        Where(l => l.Alpha3Code == border).
                //                        FirstOrDefault();
                //if (land != null)
                //{
                //    this.Borders.Add(new Border
                //    {
                //        Code = land.Alpha3Code,
                //        Name = land.Name,
                //    });
                //}
            }
        } 
        #endregion



    }
}
