﻿using Paises.Models;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net.Http;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Windows.Input;
using GalaSoft.MvvmLight.Command;
using System.Linq;

namespace Paises.ViewModel
{
    public class PaisesViewModel : BaseViewModel
    {
        #region Atributos
        private bool isRunning;
        private ObservableCollection<Models.Pais> paises;
        private bool isRefreshing;
        private string filter;
        #endregion

        #region Propiedades
        public ObservableCollection<Models.Pais> Paises
        {
            get { return paises; }
            set
            {
                if (paises != value)
                {
                    paises = value;
                    OnPropertyChanged(nameof(Paises));
                }
            }
        }

        public bool IsRefreshing
        {
            get { return isRefreshing; }
            set
            {
                if (isRefreshing != value)
                {
                    isRefreshing = value;
                    OnPropertyChanged(nameof(IsRefreshing));
                }
            }
        }
        public string Filter
        {
            get { return this.filter; }
            set
            {
                if (filter != value)
                {
                    filter = value;
                    OnPropertyChanged(nameof(Filter));
                    Search();
                }
            }
        }
        #endregion

        #region Constructores
        public PaisesViewModel()
        {
            CargarPaises();
        }

        private async void CargarPaises()
        {
            IsRefreshing = true;

            var response = await GetList<Models.Pais>(
                "http://restcountries.eu",
                "/rest",
                "/v2/all");

            if (!response.IsSuccess)
            {
                IsRefreshing = false;
                await Application.Current.MainPage.DisplayAlert(
                    "Error",
                    response.Message,
                    "OK");
                await Application.Current.MainPage.Navigation.PopAsync();
                return;
            }

            App.ListaPaises = (List<Models.Pais>)response.Result;
            Paises = new ObservableCollection<Models.Pais>(ToPaisItemViewModel());
            this.IsRefreshing = false;
        }
        #endregion

        #region Metodos
        
        public ICommand RefreshCommand
        {
            get
            {
                return new RelayCommand(CargarPaises);
            }
        }

        public ICommand SearchCommand
        {
            get
            {
                return new RelayCommand(Search);
            }
        }


        #endregion

        #region Commands
        private void Search()
        {
            if (string.IsNullOrEmpty(this.Filter))
            {
                Paises = new ObservableCollection<Models.Pais>(ToPaisItemViewModel());
            }
            else
            {
                Paises = new ObservableCollection<Models.Pais>(
                    this.ToPaisItemViewModel().Where(
                        l => l.Name.ToLower().Contains(this.Filter.ToLower()) ||
                             l.Capital.ToLower().Contains(this.Filter.ToLower())));
            }
        }

        private IEnumerable<Models.Pais> ToPaisItemViewModel()
        {
            return App.ListaPaises.Select(l => new Models.Pais
            {
                Alpha2Code = l.Alpha2Code,
                Alpha3Code = l.Alpha3Code,
                AltSpellings = l.AltSpellings,
                Area = l.Area,
                Borders = l.Borders,
                CallingCodes = l.CallingCodes,
                Capital = l.Capital,
                Cioc = l.Cioc,
                Currencies = l.Currencies,
                Demonym = l.Demonym,
                Flag = l.Flag,
                Gini = l.Gini,
                Languages = l.Languages,
                Latlng = l.Latlng,
                Name = l.Name,
                NativeName = l.NativeName,
                NumericCode = l.NumericCode,
                Population = l.Population,
                Region = l.Region,
                RegionalBlocs = l.RegionalBlocs,
                Subregion = l.Subregion,
                Timezones = l.Timezones,
                TopLevelDomain = l.TopLevelDomain,
                Translations = l.Translations,
            });
        }

        public async Task<Response> GetList<T>(
            string urlBase,
            string servicePrefix,
            string controller)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(urlBase);
                var url = string.Format("{0}{1}", servicePrefix, controller);
                var response = await client.GetAsync(url);
                var result = await response.Content.ReadAsStringAsync();

                if (!response.IsSuccessStatusCode)
                {
                    return new Response
                    {
                        IsSuccess = false,
                        Message = result,
                    };
                }

                var list = JsonConvert.DeserializeObject<List<T>>(result);
                return new Response
                {
                    IsSuccess = true,
                    Message = "Ok",
                    Result = list,
                };
            }
            catch (Exception ex)
            {
                return new Response
                {
                    IsSuccess = false,
                    Message = ex.Message,
                };
            }
        }
        #endregion
    }
}
